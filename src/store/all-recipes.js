import { writable } from "svelte/store";

function createAllRecipes() {
	const { subscribe, set, update } = writable([]);

	return {
		subscribe,
		save: recipes => set(recipes)
	};
}

export const allRecipes = createAllRecipes();
