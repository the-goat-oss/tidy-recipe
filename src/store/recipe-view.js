import { writable } from "svelte/store";

function createRecipeView() {
    const { subscribe, set } = writable("Detail");

    return {
        subscribe,
        set: (view) => set(view),
    };
}

export const recipeView = createRecipeView();