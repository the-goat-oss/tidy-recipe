import { writable } from "svelte/store";

function createSearch() {
	const { subscribe, set } = writable({ criteria: {}, results: [], initialized: false });

	return {
		subscribe,
		save: searchDetails => set(searchDetails)
	};
}

export const search = createSearch();
