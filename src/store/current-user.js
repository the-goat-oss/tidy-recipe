import { writable } from "svelte/store";

function createCurrentUser() {
  const { subscribe, set } = writable(null);

  return {
    set,
    subscribe,
    save: user => set(user)
  };
}

export const currentUser = createCurrentUser();
