import { writable } from "svelte/store";
import { format } from "date-fns";

function createAnalytics() {
  const { subscribe, update } = writable({
    initialized: false,
    pageViews: [],
    totalPageViews: 0,
    sessionId: null,
    isMobile: null,
    campaign: null,
    userId: null,
    isReturningUser: null,
    isDevelopmentOrFbApp: null,
    referrer: ""
  });

  return {
    subscribe,
    addPageView: page => {
      update(analytics => {
        const startTimestamp = new Date().getTime();
        const includeNewView = [
          ...analytics.pageViews,
          { path: page.path, name: page.name, startTimestamp }
        ];

        return {
          ...analytics,
          pageViews: includeNewView,
          totalPageViews: analytics.totalPageViews + 1
        };
      });
    },
    init: (campaign, timestamp, user, isReturningUser) => {
      update(analytics => {
        const isMobile = window.innerWidth <= 600 ? true : false;
        const date = new Date(timestamp);
        const isDevelopmentOrFbApp =
          window.location.origin === "http://localhost:3000" ||
          window.location.origin === "https://the-goat-dev.firebaseapp.com";

        return {
          ...analytics,
          initialized: true,
          sessionId: timestamp,
          date,
          formattedDate: format(new Date(timestamp), "MM/dd/yy"),
          isMobile,
          campaign,
          isReturningUser: isDevelopmentOrFbApp ? `INTERNAL` : isReturningUser,
          userId: user,
          isDevelopmentOrFbApp,
          referrer: window.document.referrer
        };
      });
    }
  };
}

export const analytics = createAnalytics();
