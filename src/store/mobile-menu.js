import { writable } from "svelte/store";

function createMobileMenu() {
  const { subscribe, set, update } = writable(false);

  return {
    subscribe,
    close: () => set(false),
    open: () => set(true),
    toggle: () => update(menu => !menu)
  };
}

export const mobileMenu = createMobileMenu();
