export const loadChartJs = () => {
    return new Promise(resolve => {
        if (window.Chart) {
            resolve()
        } else {
            let domLinkElement = document.createElement("link")

            domLinkElement.setAttribute("href", "https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.css")
            domLinkElement.setAttribute("integrity", "sha256-aa0xaJgmK/X74WM224KMQeNQC2xYKwlAt08oZqjeF0E=")
            domLinkElement.setAttribute("crossorigin", "anonymous")

            document.body.appendChild(domLinkElement)

            let domScriptElement = document.createElement("script")

            domScriptElement.setAttribute("src", "https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js")
            domScriptElement.setAttribute("integrity", "sha256-R4pqcOYV8lt7snxMQO/HSbVCFRPMdrhAFMH+vr9giYI=")
            domScriptElement.setAttribute("crossorigin", "anonymous")

            document.body.appendChild(domScriptElement)

            domScriptElement.onload = () => {
                resolve()
            };
        }
    })
}