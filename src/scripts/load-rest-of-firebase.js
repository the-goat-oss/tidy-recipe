export const loadRestOfFirebase = () => {
  const loadedFirebaseModules = [];

  return new Promise(resolve => {
    if (window.firebase.firestore && firebase.auth) {
      resolve();
    } else {
      const loadedFirebaseModules = [];

      const scripts = [
        {
          name: "auth",
          url: "https://www.gstatic.com/firebasejs/7.10.0/firebase-auth.js"
        },
        {
          name: "firestore",
          url: "https://www.gstatic.com/firebasejs/7.10.0/firebase-firestore.js"
        }
      ];

      scripts.forEach(script => {
        let domScriptElement = document.createElement("script");

        domScriptElement.setAttribute("src", script.url);
        domScriptElement.setAttribute("defer", "");

        document.body.appendChild(domScriptElement);

        domScriptElement.onload = () => {
          loadedFirebaseModules.push(script.name);
          checkIfEverythingLoaded(loadedFirebaseModules);
        };
      });

      function checkIfEverythingLoaded(modulesArray) {
        if (modulesArray.length === 2) {
          resolve(window.firebase);
        }
      }
    }
  });
};
