export const loadFirebaseApp = () => {
  //firebase app needs loaded first
  return new Promise(resolve => {
    if (window.firebase) {
      resolve();
    } else {
      let domScriptElement = document.createElement("script");

      domScriptElement.setAttribute(
        "src",
        "https://www.gstatic.com/firebasejs/7.10.0/firebase-app.js"
      );

      document.body.appendChild(domScriptElement);

      domScriptElement.onload = () => {
        resolve();
      };
    }
  });
};
