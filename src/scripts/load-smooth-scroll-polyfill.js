export const loadSmoothScrollPolyfill = () => {
    //firebase app needs loaded first
    return new Promise(resolve => {
        let domScriptElement = document.createElement("script")

        domScriptElement.setAttribute("src", "https://cdn.jsdelivr.net/npm/smoothscroll-polyfill@0.4.4/dist/smoothscroll.min.js")

        document.body.appendChild(domScriptElement)

        domScriptElement.onload = () => {
            console.log("loaded polyfill")
            resolve()
        };
    })
}
