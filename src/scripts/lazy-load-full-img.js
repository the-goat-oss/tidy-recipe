export const lazyLoadFullImg = (node, data) => {
  const loaded = new Map();

  // use bigger image file if warranted
  const fileSize = window.devicePixelRatio > 1 ? "-2x" : "-1x";

  const correctImg = `${data.path + fileSize}.${data.type}`;

  if (loaded.has(correctImg)) {
    node.setAttribute("src", correctImg);
  } else {
    const img = new Image();
    img.src = correctImg;

    // TODO: get appropriate fallback image
    // if the full size image fails to load use fallback
    // img.onerror = () => {
    //     loaded.set('img/bio-background-design.svg', img);
    //     node.setAttribute("src", 'img/bio-background-design.svg');
    // };

    img.onload = () => {
      loaded.set(correctImg, img);
      node.setAttribute("src", correctImg);
    };
  }

  return {
    destroy() {} // noop
  };
};
