export const lineChartDefaults = {
	datasets: {
		pointBackgroundColor: "rgba(255, 127, 63, 1)", // The fill color for points.
		pointBorderColor: "rgba(53, 60, 63, 1)", // The border color for points.
		pointBorderWidth: 2, // The width of the point border in pixels.
		pointHitRadius: 25, // how close you have to be to hit the hover
		pointRadius: 5, // The radius of the point shape. If set to 0, the point is not rendered.
		pointRotation: undefined, // The rotation of the point in degrees.
		pointStyle: "circle", // Style of the point. https://www.chartjs.org/docs/latest/configuration/elements.html#point-styles

		backgroundColor: "rgba(255, 127, 63, 0.75)", // The line fill color.
		borderCapStyle: "round", // Cap style of the line. See MDN.
		borderColor: "rgba(255, 127, 63, 1)", // The line color.
		borderDash: undefined, // Length and spacing of dashes. See MDN.
		borderDashOffset: undefined, // Offset for line dashes. See MDN.
		borderJoinStyle: undefined, // Line joint style. See MDN.
		borderWidth: 4, // The line width (in pixels).
		clip: undefined, // How to clip relative to chartArea. Positive value allows overflow, negative value clips that many pixels inside chartArea. 0 = clip at chartArea. Clipping can also be configured per side: clip: {left: 5, top: false, right: -2, bottom: 0}
		fill: undefined, // How to fill the area under the line. See area charts.
		lineTension: 0.35, // Bezier curve tension of the line. Set to 0 to draw straightlines. This option is ignored if monotone cubic interpolation is used.
		showLine: true, // If false, the line is not drawn for this dataset.
		spanGaps: true, // If true, lines will be drawn between points with no or null data. If false, points with NaN data will create a break in the line.

		pointHoverBackgroundColor: "rgba(53, 60, 63, 1)", // Point background color when hovered.
		pointHoverBorderColor: "rgba(255, 127, 63, 1)", // Point border color when hovered.
		pointHoverBorderWidth: 4, // Border width of point when hovered.
		pointHoverRadius: 10, // The radius of the point when hovered.

		label: "Site Vitis" // The label for the dataset which appears in the legend and tooltips.
	},

	options: {
		responsive: true,
		hover: { intersect: true },
		animation: { duration: 1000 },
		tooltips: {
			titleFontSize: 16,
			bodyFontSize: 16,
			titleMarginBottom: 15,
			titleFontFamily: "Nunito,sans-serif",
			bodyFontFamily: "Nunito,sans-serif",
			xPadding: 12,
			yPadding: 12,
			displayColors: false
		},
		legend: {
			display: false
		},
		scales: {
			yAxes: [
				{
					gridLines: {
						display: true
					},
					ticks: {
						beginAtZero: true,
						fontSize: 16,
						fontStyle: "bold",
						padding: 8
					}
				}
			],
			xAxes: [
				{
					gridLines: {
						display: true
					},
					ticks: {
						fontSize: 16,
						fontStyle: "bold",
						padding: 8,
						callback: function (tick) {
							var characterLimit = 3;
							if (tick.length >= characterLimit) {
								const singleFullDate = tick.slice(-8);
								return singleFullDate.substring(0, 5);
							}
							return tick;
						}
					}
				}
			]
		}
	}
};
