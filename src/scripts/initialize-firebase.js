export const initializeFirebase = () => {
  // TODO: update this away from goat-starter
  const firebaseConfig = {
    apiKey: "AIzaSyCNZzRPT77qKVwjJPOSNDwnwjJAcB9aWys", // replace
    authDomain: "goat-starter.firebaseapp.com",
    databaseURL: "https://goat-starter.firebaseio.com",
    projectId: "goat-starter",
    storageBucket: "goat-starter.appspot.com",
    messagingSenderId: "504528532231" // replace
  };

  return new Promise(resolve => {
    if (window.firebase.apps.length > 0) {
      resolve();
    } else {
      firebase.initializeApp(firebaseConfig);
      resolve();
    }
  });
};
