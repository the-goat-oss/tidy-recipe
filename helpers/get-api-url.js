module.exports = function (domain, customEnvironmentVariable) {
	const apiUrls = {
		local: "http://localhost:5000/tidy-recipe-test/us-central1/api",
		test: "https://us-central1-tidy-recipe-test.cloudfunctions.net/api",
		production: "https://us-central1-tidy-recipe.cloudfunctions.net/api"
	};

	const isClient = domain ? true : false;

	let currentEnv;

	if (isClient) {
		if (domain.includes("localhost")) {
			currentEnv = "local";
		} else if (domain.includes("test")) {
			currentEnv = "test";
		} else {
			currentEnv = "production";
		}
	} else {
		currentEnv = customEnvironmentVariable ? customEnvironmentVariable : "local";
	}

	return apiUrls[currentEnv];
};