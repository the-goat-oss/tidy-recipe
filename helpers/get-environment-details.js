module.exports = function () {
	const domain = typeof window !== "undefined" ? window.origin : null;

	const currentEnvironmentVariable =
		typeof process !== "undefined" && process.env.CURRENT_ENVIRONMENT
			? process.env.CURRENT_ENVIRONMENT
			: null;

	return { domain, currentEnvironmentVariable };
};
