const admin = require("firebase-admin");
const firestore = admin.firestore();

module.exports = async function (res) {
	try {
		const querySnapshot = await firestore.collection("recipes").get();
		const recipes = querySnapshot.docs.map(snapshot => snapshot.data());

		return res.status(200).json({ data: recipes });
	} catch (error) {
		res.status(400).json({ error: error.message });
	}
};
