const admin = require("firebase-admin");
const firestore = admin.firestore();

module.exports = async function (req, res) {
	const recipeDetails = req.body

	try {
		const upload = await firestore.doc("recipes/" + recipeDetails.slug).set(recipeDetails);
		return res.status(200).json({ data: upload });
	} catch (error) {
		res.status(400).json({ error: error.message });
	}

};


