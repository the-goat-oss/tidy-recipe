"use strict";
const functions = require("firebase-functions");
const admin = require("firebase-admin");

// the CURRENT_ENVIRONMENT variable is set at the CI level, so it will not exist when running locally
const shouldUseTest =
	functions.config().environment && functions.config().environment.name === "production"
		? false
		: true;

// when running locally, in order to connect to the test database, you have to manually set credentials
// this happens so that anyone can contribute to the project
// if this was for access to a production firebase project, this would be bad
if (shouldUseTest) {
	const serviceAccount = require("./tidy-recipe-test-bff27a79cb20.json");

	admin.initializeApp({
		credential: admin.credential.cert(serviceAccount),
		databaseURL: "https://tidy-recipe-test.firebaseio.com"
	});
} else {
	// you do not have to pass any config here because firebase is smart enough to know the credentials based on CI env config
	admin.initializeApp();
}

const express = require("express");
const cookieParser = require("cookie-parser")();
const cors = require("cors")({ origin: true });
const app = express();

app.use(cors);
app.use(cookieParser);

app.get("/get-all-recipes", (req, res) => {
	const getAllRecipes = require("./api/get-all-recipes");
	getAllRecipes(res);
});

app.post("/add-recipe", (req, res) => {
	const addRecipe = require("./api/add-recipe");
	addRecipe(req, res);
});

exports.api = functions.https.onRequest(app);
