# Tidy Recipe

JAMstack site for tidyrecipe.com - A recipe site that isn't terrible.

A high-level view of the tech in this monorepo.

| Category                                      | Product Used                                                          |
| --------------------------------------------- | --------------------------------------------------------------------- |
| Javascript Framework                          | [SvelteJs](https://svelte.dev) / [Sapper](https://sapper.svelte.dev/) |
| Hosting, Auth, Database, Serverless Functions | [Firebase](https://firebase.google.com/docs/web/setup)                |
| DevOps                                        | [Gitlab](https://docs.gitlab.com/ee/)                                 |
| Image Storage                                 | [Cloudinary](https://cloudinary.com/)                                 |
| Pre-build scripts                             | Node.js                                                               |

# Getting Started

This project uses two pretty default structures that you should be familiar with. [Sapper](https://sapper.svelte.dev/docs#Sapper_app_structure) for the frontend and [Firebase Cloud Functions](https://firebase.google.com/docs/functions) for the serverless backend.

Another important bit is non-standard. A set of scripts is ran on Node to generate the static json data that will be consumed by the Sapper pages. The result of this process is found in `/static/data/`

**To get running locally:**

-   If you don't yet have `firebase-tools` installed globally, `npm install firebase-tools -g`. You'll have to go through some light setup in the CLI.
-   `git clone https://gitlab.com/the-goat-oss/tidy-recipe.git`
-   `git checkout -b your-branch-name`
-   `cd tidy-recipe`
-   `npm install && cd functions && npm install && cd ..` to install all front and backend dependencies
-   `npm run dev` to spin up local environment - it emulates the serverless api locally and also the app
    -   if you get an error for no project active, run `firebase use default`
-   `npm run generate-data` you'll need to run this at least once to seed your local data that sapper will use to build recipe data for. Update as often as you need to get fresh data from the database. This requires `serve-api` to be running to complete this command

**Development Quirks:**

-   Hot reloading: Anytime you change global css, hot reloading won't work because the scss to css compile happens at build time. **Note:** All Bulma classes are available at during dev but will be stripped out for non-local builds
-   Uncommon scenario - If you make changes to the api and use it in the generate-data scripts, the api will need to be deployed first (separately or manually by maintainer) so that it the updated api is there by the time the generate scripts need it at build time

# Testing

> TODO: add things about Jest unit tests and Cypress end-to-end tests

# Contributions

Anyone can run the project locally and submit pull requests. **You should submit pull requests to the `test` branch**, as this is linked to our [test environment](https://tidy-recipe-test.firebaseapp.com). When merged by maintainers, it will run a CI pipeline at deploy out to test url.

After this is verified to be working as expected on test url, a new PR to master can be submitted.

Maintainers will manually trigger production deploys, or scheduled automated deploys will pick up any new changes in master.
