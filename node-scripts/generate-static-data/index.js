const fs = require("fs");
const formatRecipeList = require("./format-recipe-list");
const getAllRecipes = require("./get-all-recipes");
const generateJsonFile = require("../helpers/generate-json-file");

// generate the static/data folders if they don't exist yet to prevent
// an error when trying to add a file to a path that doesn't exist
if (!fs.existsSync(__dirname + `/../../static/data/`)) {
	fs.mkdirSync(__dirname + `/../../static/data/`);
}
if (!fs.existsSync(__dirname + `/../../static/data/recipes`)) {
	fs.mkdirSync(__dirname + `/../../static/data/recipes`);
}

async function generateStaticData() {
	let allRecipes = [];
	try {
		allRecipes = await getAllRecipes();
	} catch (error) {
		console.log(error)
		return;
	}

	// remove the unecessary parts of each recipe that aren't needed for searchability and add in computed properties
	const formattedRecipeList = formatRecipeList(allRecipes.data);

	generateJsonFile(formattedRecipeList, "/../../static/data/recipes", "all-recipes");

	for (recipe of allRecipes.data) {
		generateJsonFile(recipe, "/../../static/data/recipes", recipe.slug);
	}
}

generateStaticData();
