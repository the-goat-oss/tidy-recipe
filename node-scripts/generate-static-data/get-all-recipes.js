const request = require("request");
const getApiUrl = require("../../helpers/get-api-url");

const currentEnvironmentVariable = process.env.CURRENT_ENVIRONMENT || null;
const baseApiUrl = getApiUrl(null, currentEnvironmentVariable);

module.exports = function() {
	const promise = new Promise((resolve, reject) => {
		request.get(`${baseApiUrl}/get-all-recipes`, function(error, response, body) {
			if (error) {
				reject(
					new Error(
						`There was an error during api call for getting recipes with the following message: ${error.message}`
					)
				);
			} else {
				const data = JSON.parse(body);
				resolve(data);
			}
		});
	});
	return promise;
};
