module.exports = function(allRecipeData) {
	const recipesWithAddedProperties = allRecipeData.map(recipe => {
		const hasReviews = typeof recipe.reviews !== "undefined";

		return {
			...recipe,
			averageRating: hasReviews ? getAverageRating(recipe.reviews) : null,
			ratingCount: hasReviews ? recipe.reviews.length : 0,
			ingredientQuantity: recipe.instructions.length,
			totalTimeInMinutes:
				recipe.prepTimeInMinutes + recipe.cookTimeInMinutes + recipe.idleTimeInMinutes
		};
	});

	// remove properties that aren't needed for searching the recipe list
	const trimmedRecipes = recipesWithAddedProperties.map(
		({
			tips,
			additionalPhotos,
			caloriesPerServing,
			servingYield,
			prepTimeInMinutes,
			cookTimeInMinutes,
			idleTimeInMinutes,
			reviews,
			ingredients,
			instructions,
			...recipe
		}) => {
			return recipe;
		}
	);

	return trimmedRecipes;
};

function getAverageRating(reviews) {
	const average =
		reviews.reduce((ratingTotal, review) => review.rating + ratingTotal, 0) / reviews.length;

	if (Number.isInteger(average)) {
		return average;
	} else {
		return Number(parseFloat(average).toFixed(1));
	}
}
