// only run this if you need brand new data - this would potentially break end to end tests 
// const generateTestDbJson = require("./generate-test-db-json");
// generateTestDbJson();

const testData = require("./formatted-test-recipes.json");
const https = require("https");

for (let i = 0; i < testData.length; i++) {
	uploadToDatabase(testData[i]);
}

function uploadToDatabase(recipe) {
	const data = JSON.stringify(recipe);

	const options = {
		hostname: "us-central1-tidy-recipe-test.cloudfunctions.net",
		port: 443,
		path: "/api/add-recipe",
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			"Content-Length": data.length
		}
	};

	const req = https.request(options, res => {
		console.log(`statusCode: ${res.statusCode}`);

		res.on("data", d => {
			process.stdout.write(d);
		});
	});

	req.on("error", error => {
		console.error("error!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		console.error(error);
	});

	req.write(data);
	req.end();
}
