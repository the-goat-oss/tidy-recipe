const generateJsonFile = require("../helpers/generate-json-file");

// this data comes from this request: https://api.spoonacular.com/recipes/random?number=100&apiKey=e260056782ae4df4a9fe11dadc20c6df
// to get more than 100, make multiple requests and manually put together
const spoonacularRecipes = require("./raw-spoonacular-data.json");

module.exports = function () {
	// remove duplicate recipes since the api can only request 100 at a time and I glued a bunch of requests together
	const noDuplicateRecipes = spoonacularRecipes.recipes.filter(
		(recipe, index, self) => self.findIndex(obj => obj.title === recipe.title) === index
	);

	// scrub the spoonacular recipes to fit the tidy recipe data model
	const formattedRecipes = formatRecipes(noDuplicateRecipes);

	// save to a JSON file so that it can just live in the repo and be uploaded to the database
	// at any time, and still have the same exact data, as to not break any automation; otherwise,
	// the randomness of this script will return different results than what tests may be looking for
	generateJsonFile(formattedRecipes, "/../populate-test-db", "formatted-test-recipes");

	return "done"
};

function formatRecipes(rawRecipes) {
	const formattedRecipes = rawRecipes.map(recipe => {
		return {
			name: replaceQuotes(recipe.title),
			slug: slugify(recipe.title),
			author: replaceQuotes(recipe.sourceName),
			dateCreated: new Date(generateRandomDate()),
			reviews: getReviewsArray(),
			ingredients: getIngredients(recipe.extendedIngredients),
			instructions: recipe.analyzedInstructions[0]
				? getInstructions(recipe.analyzedInstructions[0].steps)
				: [],
			prepTimeInMinutes: recipe.preparationMinutes || "",
			cookTimeInMinutes: recipe.cookingMinutes || "",
			idleTimeInMinutes: getIdleTime(),
			servingYield: recipe.servings,
			caloriesPerServing: getCaloriesPerServing(),
			mainPhoto: recipe.image,
			additionalPhotos: [],
			dishType: recipe.dishTypes[0],
			tags: generateTags(),
			tips: [
				{
					text: "This is a test tip that I couldn't make good test data for.",
					relatesToStepNumber: 1
				}
			]
		};
	});

	return formattedRecipes;
}

function slugify(name) {
	const strippedName = name.replace(/[^a-zA-Z0-9 ]/g, "").toLowerCase();
	const nameWithoutSpaces = strippedName.replace(/[ ]/g, "-");

	return nameWithoutSpaces;
}

function generateRandomDate() {
	const month = generateNumberBetween(1, 13);
	const day = generateNumberBetween(1, 30);
	const year = generateNumberBetween(2017, 2020);

	return `${month}/${day}/${year}`;
}

function generateNumberBetween(min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}

function getReviewsArray() {
	const randomProbability = Math.random();

	let numberOfReviews;

	if (randomProbability < 0.25) {
		return [];
	} else if (randomProbability < 0.5) {
		numberOfReviews = 5;
	} else {
		numberOfReviews = generateNumberBetween(0, 300);
	}

	let reviews = [];

	for (let i = 0; i < numberOfReviews; i++) {
		reviews.push({
			rating: generateNumberBetween(1, 6),
			review:
				"This is just a sample review.  It was created by an elaborate AI.  Or a node script.  Whatever.",
			reviewer: "Test Reviewer"
		});
	}

	return reviews;
}

function getIngredients(rawIngredients) {
	return rawIngredients.map(ingredient => {
		return {
			name: ingredient.name,
			quantityValue: ingredient.amount,
			measurementType: ingredient.unit
		};
	});
}

function getInstructions(steps) {
	return steps.map(step => replaceQuotes(step.step));
}

// I tried to just do a big ol' replacement on the stringified version at the end, but doesn't work
function replaceQuotes(string) {
	if (!string) return
	return string.replace("’", "'")
}

function getIdleTime() {
	const randomProbability = Math.random();

	if (randomProbability < 0.9) {
		return "";
	} else {
		return generateNumberBetween(0, 60);
	}
}

function getCaloriesPerServing() {
	const randomProbability = Math.random();

	if (randomProbability < 0.5) {
		return "";
	} else {
		return generateNumberBetween(30, 500);
	}
}

function generateTags() {
	const tags = [];

	const randomProbability1 = Math.random();
	const randomProbability2 = Math.random();
	const randomProbability3 = Math.random();
	const randomProbability4 = Math.random();
	const randomProbability5 = Math.random();
	const randomProbability6 = Math.random();

	if (randomProbability1 > 0.75) tags.push("low calorie", "diet", "low carb");
	if (randomProbability2 > 0.75) tags.push("vegan", "sustainable");
	if (randomProbability3 > 0.75) tags.push("grilling", "bbq", "smoked");
	if (randomProbability4 > 0.75) tags.push("fried", "crispy", "salty");
	if (randomProbability5 > 0.75) tags.push("spicy");
	if (randomProbability6 > 0.75)
		tags.push("holiday", "theme", "cookie decorating", "valentine's day dessert");

	return tags;
}
