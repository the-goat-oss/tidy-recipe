module.exports = [
	{
		name: "Peanut Butter Cookie",
		slug: "peanut-butter-cookie",
		author: "Paul Brown",
		dateCreated: "Thu Jan 02 2020 12:51:36 GMT-0600 (Central Standard Time)",
		reviews: [
			{
				rating: 5,
				review: "It was pretty wicked",
				reviewer: "jimmyKid22"
			},
			{
				rating: 4,
				review: "Almost a 5, but not.",
				reviewer: "sarahB"
			}
		],
		ingredients: [
			{
				name: "Peanut butter",
				quantityValue: 6,
				measurementType: "tbsp"
			},
			{
				name: "Eggs",
				quantityValue: 2,
				measurementType: "whole"
			},
			{
				name: "Coconut sugar",
				quantityValue: 0.25,
				measurementType: "cups"
			}
		],
		instructions: [
			"Put all of the ingredients in a bowl and mix thoroughly",
			"Gather into balls the size of a golf ball and place evenly on baking sheet",
			"Bake at 350 for 8 minutes",
			"Remove from cookie sheet onto rack for cooling",
			"Wait at least 15 minutes, then enjoy"
		],
		prepTimeInMinutes: 5,
		cookTimeInMinutes: 5,
		idleTimeInMinutes: 15,
		servingYield: 6,
		caloriesPerServing: 150,
		mainPhoto:
			"https://food.fnr.sndimg.com/content/dam/images/food/fullset/2015/4/2/1/RF0204H_Peanut-Butter-Cookies_s4x3.jpg.rend.hgtvcom.616.462.suffix/1428427055253.jpeg",
		additionalPhotos: [
			"https://images-gmi-pmc.edge-generalmills.com/95c65352-7608-4ed1-9bd7-9ec1e17f2b4e.jpg",
			"https://assets.epicurious.com/photos/5703f1f31d4eceea190d2a9e/master/pass/EP_01212016_3ingredientpeanutbuttercookies_recipe.jpg"
		],
		dishType: "dessert",
		tags: ["sweet", "peanut butter", "cookie"],
		tips: [
			{
				text:
					"If you don't put them on a cooling rack, they will continue to cook and may not turn out perfect",
				relatesToStepNumber: 4
			}
		]
	},
	{
		name: "Roasted Chicken",
		slug: "roasted-chicken",
		author: "Paul Brown",
		dateCreated: "Thu Jan 01 2020 12:51:36 GMT-0600 (Central Standard Time)",
		ingredients: [
			{
				name: "Chicken breast",
				quantityValue: 2,
				measurementType: "lbs"
			},
			{
				name: "Famous Dave's chicken spice mix",
				quantityValue: 1,
				measurementType: "packet"
			},
			{
				name: "Olive oil",
				quantityValue: 0.25,
				measurementType: "cups"
			}
		],
		instructions: [
			"Cover the chicken breasts in the olive oil",
			"Spread the packet of seasoning onto the chicken breast with your hands, as evenly as possible.",
			"Bake at 350 for 30 minutes",
			"Let rest for 5 minutes"
		],
		prepTimeInMinutes: 5,
		cookTimeInMinutes: 30,
		idleTimeInMinutes: 5,
		servingYield: 6,
		caloriesPerServing: 150,
		mainPhoto:
			"https://www.diabetesfoodhub.org/system/thumbs/system/images/recipes/450-diabetic-roasted-chicken-breast_shutterstock332807894_012919_2959761669.jpg",
		dishType: "entree",
		tags: ["chicken", "baking"]
	}
];
