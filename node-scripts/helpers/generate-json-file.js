const fs = require("fs");

module.exports = function (data, path, fileName) {
	const dataJson = JSON.stringify(data);

	fs.writeFileSync(
		__dirname + `${path}/${fileName}.json`,
		dataJson,
		"utf8",
		function (err) {
			if (err) {
				console.log(`ERROR saving ${fileName}.json`);
				console.log(JSON.stringify(err));
			} else {
				console.log(`successfully wrote to ${path}/${fileName}.json`);
			}
		}
	);
};
